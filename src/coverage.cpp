/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include "seqio.hpp"
#include "hashmap.hpp"

#define PROGNAME "coverage"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: "PROGNAME" [-i SAM] [-o STATS]\n"
"\n"
"Parses a SAM alignment file and writes a coverage table to STATS with columns\n"
"for the reference name, the length of the referene, and the number of reads\n"
"covering it in the alignment.\n"
"\n"
"If no input files are specified, input is read from stdin.\n"
"If not output is specified, stdout is used.\n"
"\n"
"  -i  specify SAM input file\n"
"  -1  SAM input has a single reference: report coverage for each index\n"
"      in the reference\n"
"\n";
}

void coverage(const char* input_name)
{
	IntHash references;

	istream *in;
	if (!strcmp(input_name, "-"))
		in = &cin;
	else
		in = new ifstream(input_name);

	string mapline_str;
	const char *mapline;
	while (in->good())
	{
		getline(*in, mapline_str);
		mapline = mapline_str.c_str();

		// ignore comments
		if (mapline[0] == '@')
			continue;

		const char *tab = strtok((char *)mapline, "\t");
		for (int idx = 0; tab != NULL; idx++)
		{
			if (idx == 2) {
				if (references.count(tab) == 0) {
					references[tab] = 0;
				} else {
					references[tab]++;
				}
			}
			tab = strtok(NULL, "\t");
		}
	}

	references.erase("*");

	IntHash::iterator it;
	for (it = references.begin(); it != references.end(); ++it) {
			size_t pos = it->first.find_last_of('_') + 1;
			cout << it->first << "\t" << it->first.substr(pos) << "\t" << it->second << endl;
	}

	if (in != &cin)
		delete in;

}

void coverage_single(const char* input_name)
{
	vector<size_t> bases(1048576, 0);

	istream *in;
	if (!strcmp(input_name, "-"))
		in = &cin;
	else
		in = new ifstream(input_name);

	size_t max_pos = 0;

	string mapline_str;
	const char *mapline;
	while (in->good())
	{
		getline(*in, mapline_str);
		mapline = mapline_str.c_str();

		// ignore comments
		if (mapline[0] == '@')
			continue;

		const char *tab = strtok((char *)mapline, "\t");
		for (int idx = 0; tab != NULL; idx++)
		{
			if (idx == 3) {
				unsigned long pos = strtoul(tab, NULL, 10);
				bases[pos]++;
				max_pos = max(max_pos, pos);
			}
			tab = strtok(NULL, "\t");
		}
	}

	for (size_t i=0; i<=max_pos; i++) {
		cout << i << "\t" << bases[i] << endl;
	}

	if (in != &cin)
		delete in;

}

int main(int argc, char** argv)
{
	const char* input_name = NULL;
	int single = 0;

	int c;
	while ((c = getopt(argc, argv, "vhi:1")) != -1)
		switch (c) {
			case 'i':
				input_name = optarg;
				break;
			case '1':
				single = 1;
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	/* Default to stdin. */
	if (!input_name) input_name = "-";

	if (single) coverage_single(input_name);
	else coverage(input_name);

	return EXIT_SUCCESS;
}

