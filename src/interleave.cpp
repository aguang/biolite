/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "seqio.hpp"

#define PROGNAME "interleave"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: "PROGNAME" -i INPUT [...] [-o OUTPUT] [-s SEP]\n"
"\n"
"Interleaves the records in the input files (FASTA or FASTQ is automatically\n"
"detected) and writes them to OUTPUT, or to stdout if no OUTPUT is specified.\n"
"\n"
"  -i  specify multiple INPUT files\n"
"  -o  specify the OUTPUT file\n"
"  -s  reformat paired read ID lines by replacing everything after the first\n"
"      space, tab or / with the specified SEP followed by the input file number\n"
"      (e.g. '/' will use /1 as the ID suffix for the first input file, etc.)\n"
"\n"
"Example usage:\n"
"interleave -i 1.fastq -i 2.fastq -o shuffled.fastq\n"
"\n";
}

void interleave(
		vector<SeqIO*>& inputs,
		ostream* output,
		const char* sep
		)
{
	bool good = true;

	vector<size_t> nsuffix;
	vector<char*> suffix;
	if (sep != NULL) {
		for (unsigned i=0; i<inputs.size(); i++) {
			char* s = (char*)malloc(strlen(sep) + inputs.size());
			ALLOC_CHECK(s)
			sprintf(s, "%s%d", sep, i+1);
			suffix.push_back(s);
			nsuffix.push_back(strlen(s));
		}
	}

	/* Advance all of the input files. */
	for (unsigned i=0; i<inputs.size(); i++) {
		/* All inputs must have a record in order to proceed. */
		good &= inputs[i]->nextRecord();
	}

	while (good) {
		for (unsigned i=0; i<inputs.size(); i++) {
			if (sep != NULL) inputs[i]->reformatID(suffix[i], nsuffix[i]);
			inputs[i]->printRecord(*output);
			/* Advance again. */
			good &= inputs[i]->nextRecord();
		}
	}

	if (sep != NULL) {
		for (unsigned i=0; i<inputs.size(); i++) {
			free(suffix[i]);
		}
	}
}

int main(int argc, char** argv)
{
	vector<const char*> input_names;
	const char* output_name = NULL;
	const char* sep = NULL;

	int c;
	while ((c = getopt(argc, argv, "vhi:o:s:")) != -1)
		switch (c) {
			case 'i':
				input_names.push_back(optarg);
				break;
			case 'o':
				output_name = optarg;
				break;
			case 's':
				sep = optarg;
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	if (input_names.size() == 0)
		ARG_ERROR("you must specify at least one input file with -i")

	if (sep != NULL)
		NOTIFY("reformating IDs with separator '" << sep << "'")

	/* Open the input and output files. */
	vector<SeqIO*> inputs;
	ostream* output;
	for (unsigned i=0; i<input_names.size(); i++) {
		inputs.push_back(new SeqIO(input_names[i]));
		/* Default to stdout for output. */
		if (output_name == NULL) {
			output = &cout;
		} else {
			output = new ofstream(output_name);
		}
	}

	/* Perform the interleave. */
	interleave(inputs, output, sep);

	/* Cleanup file objects. */
	for (unsigned i=0; i<inputs.size(); i++)
		delete inputs[i];

	if (output != &cout)
		delete output;

	return EXIT_SUCCESS;
}

