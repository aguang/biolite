/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <omp.h>
#include <fstream>

#define PROGNAME "count_lines"
#include "util.h"

using namespace std;

#define CHUNKSIZE 1024*1024

void print_usage()
{
	cout << "\n"
"usage: "PROGNAME" [-t THREADS] [INPUT ...]\n"
"\n"
"Count the number of lines in the INPUT files using multiple threads to\n"
"increase throughput.\n"
"\n"
"If no inputs are specified, input is read from stdin.\n"
"\n"
"  -t  use THREADS (default: number of cores)\n"
"\n";
}

size_t count_lines(const char* filename, int nthreads)
{
	if (nthreads > 0)
		omp_set_num_threads(nthreads);

	size_t sumlines = 0;

	#pragma omp parallel reduction(+:sumlines)
	{
		istream* f;

		if (strcmp(filename, "-") == 0) {
			f = &cin;
		} else {
			f = new ifstream(filename, ifstream::in);
		}

		if (!f) ERROR("can't open " << filename)

		char* buf = (char*)malloc(CHUNKSIZE);
		if (buf == NULL) ERROR("could not allocate read buffer")

		size_t nlines = 0;
		size_t offset = CHUNKSIZE * omp_get_thread_num();
		size_t stride = CHUNKSIZE * omp_get_num_threads();

		/* Seek to initial offset. */
		f->seekg(offset, ios::beg);

		while (f->good())
		{
			memset(buf, 0x00, CHUNKSIZE);
			f->read(buf, CHUNKSIZE);
			for (size_t i=0; i<f->gcount(); i++) {
				/* we could use an SSE intrinsic for the comparison,
				   but the compiler should be smart enough to do this
				   for us */
				nlines += (size_t)(buf[i] == '\n');
			}
			offset += stride;
			f->seekg(offset, ios::beg);
		}

		sumlines += nlines;
	}

	return sumlines;
}

int main (int argc, char** argv)
{
	int nthreads = 0;

	int c;
	while ((c = getopt(argc, argv, "vht:")) != -1)
		switch (c) {
			case 't':
				nthreads = atoi(optarg);
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	if (optind == argc)
		printf("%zu %s\n", count_lines("-", nthreads), "-");
	else
		for (int i=optind; i<argc; i++)
			printf("%zu %s\n", count_lines(argv[i], nthreads), argv[i]);

	return EXIT_SUCCESS;
}

