/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include "seqio.hpp"

#define PROGNAME "insert_stats"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: "PROGNAME" -i SAM -o HIST -m MAX_INSERT\n"
"\n"
"Reads a SAM alignment file and uses it to estimate the mean and std. dev.\n"
"of the insert size of the mapped paired-end reads. A histogram of all insert\n"
"sizes encountered is written to the HIST file.\n"
"\n"
"  -i  specify the SAM input file\n"
"  -o  specify the STATS output file\n"
"  -m  MAX_INSERT bin allocated for the histogram\n"
"\n";
}

void print_stats(
		const char* input_name,
		const char* hist_name,
		int hist_max
		)
{
	istream *in;
	if (!strcmp(input_name, "-"))
		in = &cin;
	else
		in = new ifstream(input_name);

	ostream *hist = NULL;
	int *bins = NULL;
	int outside = 0;
	if (hist_max > 0 && hist_name != NULL)
	{
		hist = new ofstream(hist_name);
		bins = new int[hist_max+1];
	}

	long sum = 0;
	long square_sum = 0;
	long n = 0;
	long unpaired = 0;

	// Lines where insert is zero, < 0, or > 0
	long int z = 0, lz = 0, gz = 0;

	string mapline_str;
	const char *mapline;
	while (in->good())
	{
		getline(*in, mapline_str);
		mapline = mapline_str.c_str();

		// ignore comments
		if (mapline[0] == '@')
			continue;

		const char *tab = strtok((char *)mapline, "\t");
		int idx, len = 0;
		short int flags = 0;
		for (idx = 0; tab != NULL; idx++)
		{
			switch (idx)
			{
				case 1: // flags
					flags = atoi(tab);
					break;
				case 8: // fragment size
					len = atoi(tab);
					break;
				default:
					break;
			}
			tab = strtok(NULL, "\t");
		}

		if (len == 0)
			z++;
		else if (len < 0)
			lz++;
		else if (len > 0)
			gz++;

		if (flags & 0x02)
		{
			if (len < 0)
				len *= -1;

			if (len <= hist_max && bins != NULL)
				bins[len]++;
			else
				outside++;

			sum += len;
			square_sum += len*len;
			n++;
		} else {
			unpaired++;
		}
	}

	double mean = sum / (double)n;
	double stddev = sqrt(square_sum / (double)n - mean * mean);

	DIAGNOSTICS("mean", mean, "")
	DIAGNOSTICS("stddev", stddev, "")
	if (hist != NULL)
		DIAGNOSTICS("hist_file", hist_name, "")

	NOTIFY("mappings_read\t" << n+unpaired)
	NOTIFY("paired_mappings\t" << n)
	NOTIFY("unpaired_mappings\t" << unpaired)
	NOTIFY("zero_insert_mappings\t" << z)
	NOTIFY("less_zero_insert_mappings\t" << lz)
	NOTIFY("greater_zero_insert_mappings\t" << gz)
	NOTIFY("outside_histogram\t" << outside)

	if (hist != NULL && bins != NULL) {
		for (int i = 1; i < hist_max + 1; i++)
			*hist << bins[i] << endl;
		delete hist;
		delete[] bins;
	}

	if (in != &cin)
		delete in;

}

int main(int argc, char** argv)
{
	const char* input_name = NULL;
	const char* hist_name = NULL;

	int hist_max = -1;
	
	int c;
	while ((c = getopt(argc, argv, "vhi:o:m:")) != -1)
		switch (c) {
			case 'i':
				input_name = optarg;
				break;
			case 'o':
				hist_name = optarg;
				break;
			case 'm':
				hist_max = atoi(optarg);
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	if (hist_max < 0 && hist_name != NULL)
		ARG_ERROR("must specify a maximum histogram size")

	/* Default to stdin. */
	if (!input_name) input_name = "-";

	print_stats(input_name, hist_name, hist_max);

	return EXIT_SUCCESS;
}

