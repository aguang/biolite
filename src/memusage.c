/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

__attribute__ ((destructor))
void memusage()
{
	char line[128];

	/* Report memory usage from /proc/self/status interface. */
	FILE* status = fopen("/proc/self/status", "r");
	if (!status) {
		fputs("memusage: unable to open /proc/self/status\n", stderr);
		return;
	}

	long long unsigned VmPeak = 0;
	long long unsigned VmHWM = 0;
	while (!feof(status)) {
		fgets(line, 128, status);
		if (strncmp(line, "VmPeak:", 7) == 0) {
			int n = sscanf(line, "VmPeak: ""%llu", &VmPeak);
			if (n != 1) {
				fputs("memusage: could not parse VmPeak\n", stderr);
			}
		} else if (strncmp(line, "VmHWM:", 6) == 0) {
			int n = sscanf(line, "VmHWM: ""%llu", &VmHWM);
			if (n != 1) {
				fputs("memusage: could not parse VmHWM\n", stderr);
			}
			break;
		}
	}
	fclose(status);

	fprintf(stderr,
		"\n"
		"[biolite.profile] vmem=%llu (VmPeak)\n"
		"[biolite.profile] mem=%llu (VmHWM)\n",
		VmPeak, VmHWM);
	fflush(stderr);
}

