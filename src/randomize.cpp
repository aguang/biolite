/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2013 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include "seqio.hpp"

#define PROGNAME "randomize"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: "PROGNAME" [-i INPUT] [-o OUTPUT] [-r READ-ORDER] [-w WRITE-ORDER]\n"
"\n"
"Randomizes the order of sequences in each INPUT file and writes these to a\n"
"corresponding OUTPUT file. By default, a new random write order is generated\n"
"and saved to WRITE-ORDER, if specified. Alternatively, specifying a READ-ORDER\n"
"file uses that order instead of a random one.\n"
"\n"
"If no input files are specified, input is read from stdin. FASTQ or FASTA\n"
"format is automatically detected. If no output is specified, stdout is used.\n"
"\n"
"  -i  specify multiple INPUT files\n"
"  -o  specify multiple OUTPUT files\n"
"  -r  use the order in READ-ORDER instead of a random order\n"
"  -w  write the random order to WRITE-ORDER\n"
"\n"
"Example usage for randomizing paired-end data in the same order:\n"
"randomize -i 1.fastq -o 1.random.fastq -w order.txt\n"
"randomize -i 2.fastq -o 2.random.fastq -r order.txt\n"
"\n"
"Example usage for randomizing single-end data:\n"
"randomize -i s.fastq -o s.random.fastq\n"
"\n";
}

void randomize(
		const char* input_name,
		const char* output_name,
		const char* writeorder_name,
		const char* readorder_name
		)
{
	vector<string> records;
	vector<size_t> order;

	/* read the sequences */
	SeqIO fastx(input_name);
	while (fastx.nextRecord()) {
		records.push_back(fastx.getRecord());
	}
	NOTIFY("sequences read: " << records.size())

	if (readorder_name) {
		NOTIFY("reading the shuffle vector from " << readorder_name)
		ifstream readorder_file(readorder_name);
		string line;
		while (readorder_file.good()) {
			getline(readorder_file, line);
			if (line.length() > 0) {
				/* Get an int value from the characters in the string. */
				order.push_back(strtoul(line.c_str(), NULL, 10));
			}
		}
	} else {
		NOTIFY("creating shuffle vector")
		/* Create a vector of sequential integers with the same length
		   as the vector of sequences */
		for (size_t i=0; i<records.size(); i++) order.push_back(i);
		/* randomize the order */
		srand((unsigned)time(NULL));
		random_shuffle(order.begin(), order.end());
	}

	if (order.size() != records.size())
		ERROR("the number of sequences and entries in the shuffle vector are not the same!")
		
	if (writeorder_name) {
		NOTIFY("writing the shuffle vector to " << writeorder_name)
		ofstream writeorder_file(writeorder_name);
		for (size_t i=0; i<order.size(); i++) {
			writeorder_file << order[i] << endl;
		}
		writeorder_file.close();
	}

	/* write the sequences to a file or to stdout */
	if (output_name) {
		NOTIFY("writing the sequences in random order to " << output_name)
		ostream* output = SeqIO::openOutput(output_name);
		for (size_t i=0; i<records.size(); i++) {
			*output << records[order[i]];
		}
		delete output;
	} else {
		for (size_t i=0; i<records.size(); i++) {
			cout << records[order[i]];
		}
	}
}

int main(int argc, char** argv)
{
	const char* input_name = NULL;
	const char* output_name = NULL;
	const char* writeorder_name = NULL;
	const char* readorder_name = NULL;
	
	int c;
	while ((c = getopt(argc, argv, "vhi:o:w:r:")) != -1)
		switch (c) {
			case 'i':
				input_name = optarg;
				break;
			case 'o':
				output_name = optarg;
				break;
			case 'w':
				writeorder_name = optarg;
				break;
			case 'r':
				readorder_name = optarg;
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	/* Use stdin if no input was specified. */
	if (!input_name) input_name = "-";

	randomize(input_name, output_name, writeorder_name, readorder_name);

	return EXIT_SUCCESS;
}

