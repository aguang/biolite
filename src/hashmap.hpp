/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BIOLITE_HASHMAP_H__
#define __BIOLITE_HASHMAP_H__

#include <string>
#include <vector>

/* Workaround for problem with Intel 11.x and 12.0 compilers:
   http://software.intel.com/en-us/forums/showthread.php?t=65041 */
/*#define __aligned__   ignored
#include <tr1/type_traits>
#undef __aligned__*/

#include <tr1/unordered_map>
#include <tr1/unordered_set>

/* Hash-backed maps provide O(1) queries. */
typedef std::tr1::unordered_set<std::string> StringSet;
typedef std::tr1::unordered_map<std::string, std::string> StringHash;
typedef std::tr1::unordered_map<std::string, int> IntHash;
typedef std::tr1::unordered_map<std::string, std::vector<int> > IntListHash;
typedef std::tr1::unordered_map<int, std::vector<int> > IntListInt;

#endif
