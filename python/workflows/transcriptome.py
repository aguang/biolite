#!/usr/bin/env python
#
# BioLite - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of BioLite.
# 
# BioLite is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# BioLite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with BioLite.  If not, see <http://www.gnu.org/licenses/>.

import os

from biolite import diagnostics
from biolite import utils
from biolite import wrappers
from biolite import wrappers2

def quantify(assembly, name, reads, outdir, workdir=os.getcwd()):
	"""
	"""

	prefix = os.path.join(workdir, name)
	wrappers.RsemReference(assembly, prefix)

	# Could do our own mapping in the future:
	# bowtie -p 2 -n 2 -l 25 -e 99999999 -m 200 ref -1 input1 -2 input2 -S out

	# Bowtie2 - RSEM doesn't support gapped alignments
	#   (https://groups.google.com/forum/#!topic/rsem-users/l9vzo4HGSeQ)
	# "--all --ignore-quals -N 1"

	name = os.path.join(outdir, name)
	wrappers2.RsemExpression(reads, prefix, name)

	genes = name + '.genes.results'
	isoforms = name + '.isoforms.results'

	diagnostics.log_path(genes, 'genes')
	diagnostics.log_path(isoforms, 'isoforms')

	return genes


def fpkms(coverage_table):
	"""
	"""

	fpkms = {}

	with open(coverage_table) as f:
		next(f) # burn the header line
		for line in f:
			fields = line.rstrip().split()
			assert len(fields) == 7
			fpkms[fields[0]] = float(fields[6])

	return fpkms

