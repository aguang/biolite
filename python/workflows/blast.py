#!/usr/bin/env python
#
# BioLite - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of BioLite.
# 
# BioLite is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# BioLite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with BioLite.  If not, see <http://www.gnu.org/licenses/>.

import math
import os

from Bio import SeqIO
from collections import namedtuple, OrderedDict

from biolite import utils


def split_query(fasta, command, chunk_size=10000, workdir=os.getcwd()):
	"""
	"""

	utils.safe_mkdir(workdir)

	commands = os.path.join(workdir, 'commands.sh')
	chunk_id = 0
	chunk_file = None

	nbases = 0

	with open(commands, 'w') as f:
		for record in SeqIO.parse(open(fasta), 'fasta'):
			nbases += len(record.seq)
			if nbases > (chunk_id * chunk_size):
				chunk_id = nbases / chunk_size + 1
				chunk_file = open(os.path.join(workdir, '%d.fa' % chunk_id), 'w')
				print >>f, command, '-query', '%d.fa' % chunk_id
			SeqIO.write(record, chunk_file, 'fasta')

	return commands


def evalue_hist(hits):
	"""
	"""

	hist = {}

	for hit in hits:
		i = int(math.log10(hits.evalue))
		hist[i] = hist.get(i, 0) + 1

	return hist


tabular_fields = "qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore stitle"

TabularHit = namedtuple('TabularHit', tabular_fields)

def hits_tabular(filename, nlimit=1):
	"""
	Similar to `blast_top_hits` but uses BLAST custom tabular format as input.
	"""
	qseqid = None
	rank = 0
	# Loop over the blast records.
	for line in open(filename):
		hit = TabularHit(*line.rstrip().split('\t'))
		if (hit.qseqid != qseqid):
			qseqid = hit.qseqid
			rank = 0
		if rank < nlimit:
			yield hit
		rank += 1


def top_hits_tabular(filename):
	"""
	Similar to `hits_tabular`, but returns an OrderedDict keyed by query name
	with only one hit (the top hit) per query.
	"""
	hits = OrderedDict()
	for hit in hits_tabular(filename, nlimit=1):
		hits[hit.qseqid] = hit
	return hits


def annotate(hits, input, output, fpkms={}):
	"""
	Iterates through the records in `input` and looks for a hit in a dict of
	BlastHit objects, `hits`.  For each record with a hit, the FPKM (if
	provided), hit title, and evalue are added to the ID. Records are written
	back out to `output`.
	"""
	with open(output, 'w') as f:
		for seq in SeqIO.parse(open(input), 'fasta'):
			hit = hits.get(seq.id)
			new_id = [str(seq.id).strip().replace('|', '_')]
			seq.description = ''
			if seq.id in fpkms:
				new_id += ('FPKM', '%.2f' % fpkms[seq.id])
			if hit:
				new_id += ('BlastHit', hit.stitle, hit.evalue)
			seq.id = '|'.join(new_id)
			SeqIO.write(seq, f, 'fasta')

