Installation
============

For quick installation instructions for OS X and Ubuntu, see the
`BioLite homepage <https://bitbucket.org/caseywdunn/biolite>`__. This
file has more detailed instructions for installation on other platforms
or for developers.

After installation, proceed to the configuration instructions at the end
of this document.

If you would like to install to a location other than ``/usr/local`` (if
you don't have permission to write to ``/usr/local``, for example), see
the "Installing to an alternative location" section.

Prerequisites
-------------

This section lists required and optional prerequisites, with notes on
specific versions we have tested and found to work.

To compile and install BioLite, you must at a minimum have:

-  A C/C++ compiler that supports OpenMP and the TR1 standard. Tested:

   -  gcc 4.4.6 (CentOS 6.3)
   -  gcc 4.6.3 (Ubuntu 12.04)
   -  XCode gcc 4.2.1 (OS X 10.8)

-  Python (2.7.2, 2.7.3) with packages:

   -  biopython (1.60, 1.61)
   -  dendropy (3.12.0)
   -  docutils (0.9.1, 0.10)
   -  matplotlib (1.1.0, 1.1.1rc, 1.1.1)
   -  networkx (1.6, 1.7)
   -  numpy (1.6.1, 1.6.2)
   -  lxml (3.2.1)
   -  wget (2.0)

BioLite provides a large collection of wrappers for the following 3rd
party bioinformatics tools. While you do not have to install these to be
able to load the BioLite python library or to use the BioLite
command-line tools, a BioLite script that calls a wrapper must be able
to find the corresponding program in your PATH. BioLite comes with a
shell script to automate downloading and building many of these 3rd
party programs. See the "Installing 3rd Party Software" section below
for more details. Alternatively, you can install these packages
manually. If you are using a shared or research computing system, it is
possible that many of these packages are already available and you will
not need to install them. At runtime, BioLite will automatically attempt
to find installed versions of these packages using your PATH.

-  `FastQC
   0.10.0 <http://www.bioinformatics.babraham.ac.uk/projects/fastqc/>`__

-  `Blast+ 2.2.27 <http://blast.ncbi.nlm.nih.gov/>`__

-  `Bowtie 0.12.8 <http://bowtie-bio.sourceforge.net/>`__

-  `Bowtie2 2.0.6 <http://bowtie-bio.sourceforge.net/bowtie2/>`__

-  `samtools 0.1.18 <http://samtools.sourceforge.net/>`__

-  `Velvet 1.2.08 <http://www.ebi.ac.uk/~zerbino/velvet/>`__

Note: with LONGSEQUENCES=1 and MAXKMERLENGTH >= 61, recommended 127

-  `Oases 0.2.08 <http://www.ebi.ac.uk/~zerbino/oases/>`__

Note: with LONGSEQUENCES=1 and MAXKMERLENGTH >= 61, recommended 127

-  `Trinity r2012-10-05 <http://trinityrnaseq.sourceforge.net/>`__

-  `MACSE 0.9b1 <http://mbb.univ-montp2.fr/macse/>`__

Note: if you install manually, make sure the MACSE jar file is in your
PATH.

-  `RAxML 7.2.8-ALPHA <http://sco.h-its.org/exelixis/software.html>`__

-  `Gblocks
   0.91b <http://molevol.cmima.csic.es/castresana/Gblocks.html>`__

-  `mcl 12-135 <http://micans.org/mcl>`__

-  `SGA 0.9.42 <https://github.com/jts/sga>`__

-  `CGAL 0.9.6-beta <http://bio.math.berkeley.edu>`__

-  `SRA
   Toolkit <http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=std>`__

Generic instructions for installing from the tar ball
-----------------------------------------------------

Download the
`tarball <https://bitbucket.org/caseywdunn/biolite/downloads>`__. Then
unpack it:

::

    tar xf biolite-X.X.X.tar.gz
    cd biolite-X.X.X

Build and install 3rd party tools and bioLite:

::

    sudo ./build_3rd_party.sh /usr/local
    ./configure
    make
    sudo make install

Proceed to "Configuration" at the end of this document.

Installing from the git repo
----------------------------

(Skip this unless you are building a development version that you cloned
from Bitbucket.)

Fork the repository and clone the fork to your machine.

On Ubuntu, you can can install BioLite and its dependencies from the
local git repository by running:

::

    sudo sh install_biolite_ubuntu.sh

For other systems, see the Prerequisites section above for other
software you may need to install. You will also need to have the
automake, autoconf and libtool packages installed. Then run:

::

    sudo ./build_3rd_party.sh /usr/local
    ./autogen.sh
    ./configure
    make
    sudo make install

Proceed to "Configuration" at the end of this document.

Installing to an alternative location
-------------------------------------

The instructions above assume that you are installing BioLite to
``/usr/local``, which requires root access. If you are installing to
another location, modify the installation instructions as follows. These
are not full instructions, they just explain how to modify the
instructions above.

In the instructions below, we use ``[installation_path]`` as a
placeholder for the path that you would like to install to, e.g.
``/home/lucy/local``. Note that ``[installation_path]`` needs to be an
absolute path.

Modify the command to build and install 3rd party tools to specify the
alternative path:

::

    ./build_3rd_party.sh [installation_path]

Modify the configure command to both change the install location and
tell it where third party packages were installed:

::

    ./configure --prefix=[installation_path]
    make
    make install

If you are installing somewhere that you have write access to, you don't
need to use sudo for ``build_3rd_party.sh`` or ``make install``.

Proceed to the next section for instructions on setting paths.

Setting PATH and PYTHONPATH
---------------------------

If you install to a canonical location on your system, like
``/usr/local``, the scripts and programs will already be in your PATH
and the python module will be ready to import.

Otherwise, if you want to be able to call the programs without
specifying their full path, you need to add the new 'bin' directory to
your PATH. In ``bash`` (you can add add this to ``~/.bashrc``):

::

    export PATH=[installation_path]/bin:$PATH

or in ``csh``:

::

    setenv PATH [installation_path]/bin:$PATH

To be able to import BioLite's python modules in python, you will also
need to add the full path to you ``PYTHONPATH``, replacing the python
version below with your version of python (most likely "2.7"). In
``bash``:

::

    export PYTHONPATH=[installation_path]/lib/python2.7/site-packages:$PYTHONPATH

or in ``csh``:

::

    setenv PYTHONPATH [installation_path]/lib/python2.7/site-packages:$PYTHONPATH

Installing 3rd Party Software
-----------------------------

The BioLite source comes with a shell script that will download and
install much of the required 3rd party software. The usage for the
script is:

::

    ./build_3rd_party.sh -h
    usage: build_3rd_party.sh [PREFIX] [CC] [CXX] [OPT]

**NOTE**: We use this script internally to install and test BioLite, and
we have only tested it on our own systems. It is likely that you will
need to manually install additional dependencies on OS X (or use a
system like homebrew), or install additional packages on Linux through
your distro's package manager (especially some development packages that
end in -dev or -devel).

To install to ``/usr/local/``, you can call the script with no
arguments. To use a different install path, specify a PREFIX, for
instance your home directory:

::

    ./build_3rd_party.sh $HOME

If you want to specify a different compiler, use the CC and CXX options.
For instance, your linux distro may have a gcc 4.6 package that installs
'gcc46', so you would use:

::

    ./build_3rd_party.sh /usr/local gcc46 g++46

Finally, if you want to specify more aggressive compiler optimizations,
use the OPT option. If you have a newer CPU that supports SSE4.2
instructions (e.g. Intel Nehalem), you could use:

::

    ./build_3rd_party.sh /usr/local gcc g++ -msse4.2

Generating a tarball from the git repo
--------------------------------------

The included release.sh script will update the git version, rebuild the
documentation and run the necessary configure/make commands to create a
tar ball.

To build the documentation, you must install the 'sphinx' Python
package, for instance with:

::

    sudo pip install sphinx

or:

::

    sudo easy_install sphinx

You must also install the
`pandoc <http://johnmacfarlane.net/pandoc/installing.html>`__ utility
for document conversion.

Configuration
=============

After successfully installing BioLite with ``make install``, you should
see a message like:

::

    |------------------------------------------------------------------------------
    | BioLite has been installed to /usr/local
    |
    | Your default configuration file is located at:
    |
    |   /usr/local/share/biolite/biolite.cfg
    |
    |------------------------------------------------------------------------------

pointing to the location of your default BioLite configuration file.
This file serves as the default configuration for any user on the
system. To override it on a per-user basis, simply copy the file to
``$HOME/.biolite/biolite.cfg`` and make any required changes.

You can also override the location of the configuration file with an
environment variable. In ``bash``:

::

    export BIOLITE_CONFIG=/your/path/to/biolite.cfg

or in ``csh``:

::

    setenv BIOLITE_CONFIG /your/path/to/biolite.cfg

Finally, the BIOLITE\_RESOURCE environment variable allows you to
temporarily override specific values in the resources section of the
configuration. For instance, if your configuration file is set to 2
threads, but want to test out a run with 8 threads instead, you could
use (in ``bash``):

::

    export BIOLITE_RESOURCES="threads=8"

The value of this variable can be a comma-separated list of
``key=value`` pairs.
