Automating workflows
====================

:mod:`workflows` Module
-----------------------

.. automodule:: biolite.workflows
    :members:
    :undoc-members:
    :show-inheritance:

